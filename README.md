Hi there.

This project demonstrates a simple example on how to use Apache Thrift.


https://thrift.apache.org/tutorial/
From the tutorial given in the link, you will be able to install Apache Thrift which can be used to generate Thrift equivalent language specific files. For instance, you will be able to install the Thrift java compiler to compile the Thrift files written IDL(Interface Definition Language) and generate Java equivalent files.

In our example here, we want to create a Calculator Service using Thrift. Then we make a call to this Thrift service using a Thrift client.

The Calculator service is described in "calculator.thrift" file.
You can see that it exposes 4 methods, add, subtract, multiply and divide. Once you use a language specific compiler (say Thrift Java compiler), you can create the "CalculatorService.java" file.
To learn more about how to compile Thrift files and generate Java equivalent files, look here
https://wiki.apache.org/thrift/ThriftGeneration

To implement the service and provide business logic, you will now need to create "CalculatorServiceImpl.java" which implements the "CalculatorService.Iface" class. Once done, you can spin up the thrift server by running the main method in "CalculatorServiceImpl.java"

Lastly, to make requests to this Calculator Thrift server which accepts requests to add, subtract, multiply and divide, look at "CalculatorClient.java" which makes the above 4 requests to the Calculator Thrift service.