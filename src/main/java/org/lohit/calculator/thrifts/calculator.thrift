namespace java org.lohit.org.lohit.calculator.calculator

service CalculatorService {
    double add (1:double a, 2:double b),
    double subtract (1:double a, 2:double b),
    double multiply (1:double a, 2:double b),
    double divide (1:double a, 2:double b),
}