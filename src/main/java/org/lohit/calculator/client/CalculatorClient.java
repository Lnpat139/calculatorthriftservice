package org.lohit.calculator.client;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.lohit.calculator.server.CalculatorService;

/**
 * Created by lpatil on 5/5/17.
 */
public class CalculatorClient {

    public static void main(String[] args) throws TException {
        TTransport transport = new TSocket("localhost", 9888);
        TProtocol protocol = new TBinaryProtocol(transport);
        CalculatorService.Client client = new CalculatorService.Client(protocol);
        transport.open();

        System.out.println(client.add(12, 48));
        System.out.println(client.subtract(100, 1));
        System.out.println(client.multiply(12, 100));
        System.out.println(client.divide(19, 5));

        transport.flush();
        transport.close();
    }
}
