package org.lohit.calculator.server;

import org.apache.thrift.TException;
import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TThreadPoolServer;
import org.apache.thrift.transport.TServerSocket;

/**
 * Created by lpatil on 5/4/17.
 */
public class CalculatorServiceImpl implements CalculatorService.Iface {

    public CalculatorServiceImpl() {}

    @Override
    public double add(double a, double b) throws TException {
        return a+b;
    }

    @Override
    public double subtract(double a, double b) throws TException {
        return a-b;
    }

    @Override
    public double multiply(double a, double b) throws TException {
        return a*b;
    }

    @Override
    public double divide(double a, double b) throws TException {
        return a/b;
    }

    public static void main(String[] args) throws TException {
        CalculatorServiceImpl calculator = new CalculatorServiceImpl();
        TServerSocket serverTransport = new TServerSocket(9888);
        CalculatorService.Processor processor = new CalculatorService.Processor(calculator);
        TThreadPoolServer.Args args1 = new TThreadPoolServer.Args(serverTransport);
        args1.processor(processor);
        TServer server = new TThreadPoolServer(args1);

        System.out.println(calculator.add(12, 48));
        System.out.println("Started service successfully...");
        server.serve();
    }
}
